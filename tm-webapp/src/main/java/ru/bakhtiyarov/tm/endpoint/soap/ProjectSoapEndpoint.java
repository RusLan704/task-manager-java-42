package ru.bakhtiyarov.tm.endpoint.soap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.converter.IProjectConverter;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

//http://localhost:8080/ws/ProjectEndpoint?wsdl

@Component
@WebService
public class ProjectSoapEndpoint {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IProjectConverter projectConverter;

    @NotNull
    @Autowired
    public ProjectSoapEndpoint(
            @NotNull final IProjectService projectService,
            @NotNull final IProjectConverter projectConverter
    ) {
        this.projectService = projectService;
        this.projectConverter = projectConverter;
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public ProjectDTO createProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @Nullable final ProjectDTO projectDTO
    ) {
        @Nullable final Project project = projectService.create(
                UserUtil.getAuthUserId(),
                projectConverter.toEntity(projectDTO)
        );
        return projectConverter.toDTO(project);
    }

    @NotNull
    @WebMethod
    @SneakyThrows
    public List<ProjectDTO> findAllProjects() {
        List<Project> projects = projectService.findAll(UserUtil.getAuthUserId());
        return projects
                .stream()
                .map(projectConverter::toDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public ProjectDTO findProjectById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @Nullable final Project project = projectService.findOneById(UserUtil.getAuthUserId(), id);
        return projectConverter.toDTO(project);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public ProjectDTO updateProjectById(
            @WebParam(name = "projectDTO", partName = "projectDTO") @Nullable final ProjectDTO projectDTO
    ) {
        @Nullable final Project project = projectService.updateProjectById(
                UserUtil.getAuthUserId(),
                projectConverter.toEntity(projectDTO)
        );
        return projectConverter.toDTO(project);
    }

    @WebMethod
    @SneakyThrows
    public void removeProjectOneById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        projectService.removeOneById(UserUtil.getAuthUserId(), id);
    }

}