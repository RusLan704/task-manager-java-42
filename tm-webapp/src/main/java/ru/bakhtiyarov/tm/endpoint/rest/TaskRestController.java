package ru.bakhtiyarov.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.dto.TaskDTO;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.service.converter.TaskConverter;
import ru.bakhtiyarov.tm.util.UserUtil;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/rest/task")
public class TaskRestController {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final TaskConverter taskConverter;

    @NotNull
    @Autowired
    public TaskRestController(@NotNull final ITaskService taskService, @NotNull TaskConverter taskConverter) {
        this.taskService = taskService;
        this.taskConverter = taskConverter;
    }

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public void create(
            @RequestBody @Nullable final TaskDTO taskDTO
    ) {
        taskService.create(UserUtil.getAuthUserId(), taskConverter.toEntity(taskDTO), taskDTO.getProjectId());
    }

    @NotNull
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> findAll() {
        List<Task> tasks = taskService.findAll(UserUtil.getAuthUserId());
        return tasks
                .stream()
                .map(taskConverter::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/findById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO findById(
            @PathVariable("id") @Nullable final String id
    ) {
        final @Nullable Task task = taskService.findOneById(UserUtil.getAuthUserId(), id);
        return taskConverter.toDTO(task);
    }

    @PutMapping(value = "/updateById", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO updateTaskById(
            @RequestBody @Nullable final TaskDTO taskDTO
    ) {
        final @NotNull Task task = taskService.updateTaskById(UserUtil.getAuthUserId(), taskConverter.toEntity(taskDTO));
        return taskConverter.toDTO(task);
    }

    @DeleteMapping(value = "/removeById/{id}")
    public void removeOneById(
            @PathVariable("id") @Nullable final String id
    ) {
        taskService.removeOneById(UserUtil.getAuthUserId(), id);
    }

}