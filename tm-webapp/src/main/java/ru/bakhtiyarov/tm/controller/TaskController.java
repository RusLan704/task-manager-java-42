package ru.bakhtiyarov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.api.service.converter.IProjectConverter;
import ru.bakhtiyarov.tm.dto.CustomUser;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.dto.TaskDTO;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.enumeration.Status;
import ru.bakhtiyarov.tm.service.converter.TaskConverter;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IProjectConverter projectConverter;

    @NotNull
    private final TaskConverter taskConverter;

    @NotNull
    @Autowired
    public TaskController(
            @NotNull final ITaskService taskService,
            @NotNull final IProjectService projectService,
            @NotNull final IProjectConverter projectConverter,
            @NotNull final TaskConverter taskConverter
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.projectConverter = projectConverter;
        this.taskConverter = taskConverter;
    }

    @ModelAttribute("statuses")
    public Status[] getStatus() {
        return Status.values();
    }

    @ModelAttribute("projects")
    private List<ProjectDTO> getProjects(
            @AuthenticationPrincipal final CustomUser user
    ) {
        List<Project> projects = projectService.findAll(user.getUserId());
        return projects
                .stream()
                .map(projectConverter::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/list")
    public ModelAndView list(
            @AuthenticationPrincipal final CustomUser user
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-list");
        List<TaskDTO> tasks = taskService
                .findAll(user.getUserId())
                .stream()
                .map(taskConverter::toDTO)
                .collect(Collectors.toList());
        modelAndView.addObject("tasks", tasks);
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-create");
        modelAndView.addObject("task", new TaskDTO());
        return modelAndView;
    }

    @PostMapping("/create")
    public String create(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") TaskDTO task
    ) {
        taskService.create(user.getUserId(), taskConverter.toEntity(task), task.getProjectId());
        return "redirect:/tasks/list";
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable(value = "id") @NotNull final String id
    ) {
        taskService.removeOneById(user.getUserId(), id);
        return new ModelAndView("redirect:/tasks/list");
    }

    @GetMapping("/update/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-update");
        @Nullable final Task task = taskService.findOneById(user.getUserId(), id);
        modelAndView.addObject("task", taskConverter.toDTO(task));
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public String update(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") TaskDTO task
    ) {
        taskService.updateTaskById(user.getUserId(), taskConverter.toEntity(task));
        return "redirect:/tasks/list";
    }

}