package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.User;
import ru.bakhtiyarov.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    Project create(
            @Nullable final String userId,
            @Nullable final Project project
    );

    @NotNull
    List<Project> findAll(@Nullable final String userId);

    @NotNull
    List<Project> findAll();

    @Nullable
    Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    );

    @Nullable
    Project findOneByIndex(
            @Nullable final String userId,
            @Nullable Integer index
    );

    @Nullable
    Project findOneByName(
            @Nullable final String userId,
            @Nullable String name
    );

    @Nullable
    Project findById(@Nullable final String id);

    @Nullable
    Project updateProjectById(
            @Nullable final String userId,
            @Nullable final Project project
    );

    void removeAll();

    void removeAll(@Nullable final String userId);

    void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    );

    void removeOneById(@Nullable String id);

    void removeOneById(@Nullable final String userId, @Nullable final String id);

    void removeOneByName(@Nullable final String userId, @Nullable String name);

}
