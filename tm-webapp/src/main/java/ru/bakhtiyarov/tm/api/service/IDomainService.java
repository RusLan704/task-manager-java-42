package ru.bakhtiyarov.tm.api.service;


public interface IDomainService {

    void loadBase64() throws Exception;
    
    void clearBase64() throws Exception;
    
    void saveBase64() throws Exception;

    void clearBinary() throws Exception;

    void loadBinary() throws Exception;

    void saveBinary() throws Exception;

    void clearJson() throws Exception;

    void loadJson() throws Exception;

    void saveJson() throws Exception;

    void clearXml() throws Exception;

    void loadXml() throws Exception;

    void saveXml() throws Exception;

    void clearYaml() throws Exception;

    void loadYaml() throws Exception;

    void saveYaml() throws Exception;

}
