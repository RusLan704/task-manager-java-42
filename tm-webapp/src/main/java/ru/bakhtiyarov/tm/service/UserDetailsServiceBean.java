package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.bakhtiyarov.tm.dto.CustomUser;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.repository.IUserRepository;

import javax.annotation.PostConstruct;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        @Nullable final User user = userRepository.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .roles(user.getRole().toString())
                .password(user.getPasswordHash())
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    public void initUsers() {
        if (userRepository.count() > 0) return;
        create("test", "test");
        create("admin", "admin");
    }

    public void create(final String login, String pass) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(pass));
        userRepository.save(user);
    }

}