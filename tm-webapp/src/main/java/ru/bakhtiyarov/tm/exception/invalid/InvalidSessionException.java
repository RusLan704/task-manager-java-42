package ru.bakhtiyarov.tm.exception.invalid;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class InvalidSessionException extends AbstractException {

    @NotNull
    public InvalidSessionException() {
        super("Error! Session is invalid...");
    }

}