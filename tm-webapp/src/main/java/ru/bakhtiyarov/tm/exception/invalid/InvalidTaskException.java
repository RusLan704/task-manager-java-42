package ru.bakhtiyarov.tm.exception.invalid;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class InvalidTaskException extends AbstractException {

    @NotNull
    public InvalidTaskException() {
        super("Error! Task is invalid...");
    }

}

