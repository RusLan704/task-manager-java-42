package ru.bakhtiyarov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyMiddleNameException extends AbstractException {

    @NotNull
    public EmptyMiddleNameException() {
        super("Error! Middle name is empty...");
    }

}