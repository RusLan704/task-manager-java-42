/*
 * Swagger Maven Plugin Sample
 * This is a sample for swagger-maven-plugin
 *
 * OpenAPI spec version: v1
 * Contact: ruslan@super.ru
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.Project;
import io.swagger.client.model.ProjectDTO;
import io.swagger.client.model.Task;
import io.swagger.client.model.TaskDTO;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for DefaultApi
 */
@Ignore
public class DefaultApiTest {

    private final DefaultApi api = new DefaultApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void createTest() throws ApiException {
        ProjectDTO body = null;
        Project response = api.create(body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void create_0Test() throws ApiException {
        TaskDTO body = null;
        Task response = api.create_0(body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void findAllTest() throws ApiException {
        List<ProjectDTO> response = api.findAll();

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void findAll_0Test() throws ApiException {
        List<TaskDTO> response = api.findAll_0();

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void findByIdTest() throws ApiException {
        String id = null;
        ProjectDTO response = api.findById(id);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void findById_0Test() throws ApiException {
        String id = null;
        TaskDTO response = api.findById_0(id);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void removeOneByIdTest() throws ApiException {
        String id = null;
        api.removeOneById(id);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void removeOneById_0Test() throws ApiException {
        String id = null;
        api.removeOneById_0(id);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void updateProjectByIdTest() throws ApiException {
        ProjectDTO body = null;
        ProjectDTO response = api.updateProjectById(body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void updateTaskByIdTest() throws ApiException {
        TaskDTO body = null;
        TaskDTO response = api.updateTaskById(body);

        // TODO: test validations
    }
    
}
