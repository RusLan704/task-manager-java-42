package io.swagger.tm_client.exception.system;

import org.jetbrains.annotations.NotNull;
import io.swagger.tm_client.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    @NotNull
    public UnknownCommandException(@NotNull final String arg) {
        super("Error! Command ``" + arg + "`` doesn't exist...");
    }

    @NotNull
    public UnknownCommandException() {
        super("Error! Command doesn't exist...");
    }

}