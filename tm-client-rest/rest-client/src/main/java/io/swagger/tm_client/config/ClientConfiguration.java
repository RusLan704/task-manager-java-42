package io.swagger.tm_client.config;

import io.swagger.client.api.DefaultApi;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("io.swagger.tmclient")
public class ClientConfiguration {

    @Bean
    @NotNull
    public DefaultApi api () {
        return new DefaultApi();
    }

}