package io.swagger.tm_client.listener.auth;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import io.swagger.tm_client.event.ConsoleEvent;
import io.swagger.tm_client.listener.AbstractListener;
import io.swagger.tm_client.util.TerminalUtil;

@Component
public class UserLoginListener extends AbstractListener {

    @NotNull
    @Autowired
    private DefaultApi api;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login user.";
    }

    @EventListener(condition = "@userLoginListener.name() == #event.name")
    public void handler(final ConsoleEvent event) throws ApiException {
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = TerminalUtil.nextLine();
        if (!api.login(login, password)) {
            System.out.println("FAIL");
            return;
        }
        System.out.println("[OK]");
    }

}
